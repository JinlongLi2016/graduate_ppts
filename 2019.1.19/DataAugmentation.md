# Data Augmentation

## 基本方法

* Random Cropping 随机裁剪  
* Mirroring 镜像  
* Rotation 旋转
* Scaling 尺度  
* Color Jitter 颜色抖动  
* Saturation and Value Jitter 饱和度和值  
* Noise 噪声  
* Brightness 明暗度  

## 方法分类  
对于有掩膜的情况，某些掩膜需要和图片保持一致性，某些数据增强方式会破环这种一致性，比如旋转。可以以此为依据将其分为两类 

|影响掩膜的方式|不影响掩膜的方式|
| :----:| :-----:|
|随机裁剪|颜色抖动(Color Jitter)|
|镜像|饱和度 和 明亮程度改变|
|旋转|噪声|
|平移|图片明亮程度发生改变|
|尺度变换||



