# 数据扩增技法(Data Augmentation Techniques)

# 优点/缺点


## Where do we do data augmentation?

off the line & on the line

How to simulate translation, viewpoint, size and illumination change?  
What kind of data augmentation techniques are necessary?

# Types

* 翻转(左右/上下)
* 旋转(Rotate)
* 尺度变换(Scale)
* 裁剪(Crop)
* 平移变换(Translation)
* 噪声(高斯/椒盐)
* jitter(抖动) 
* Seam Carving
* Perspective Transform
* GAN
* more sophisticated...


## 翻转(flip)
```
flip = np.fliplr(img)

shape = [h, w, c]
x = tf.placeholder(dtype=tf.float32, shape = shape)
flip = tf.image.flip_up_down(x)
tf.image.flip_left_right()/tf.image.random_flip_up_down(x)
```

## 旋转(Rotate)




## [尺度变换(Scale)](http://scikit-image.org/docs/dev/api/skimage.transform.html#skimage.transform.rescale)

旋转角度及旋转后shape的处理办法  
<a href="http://scikit-image.org/docs/dev/api/skimage.transform.html#skimage.transform.rescale">Skimage.transfrom.rescale</a>

```
Scikit Image 

scale_out = skimage.transform.rescale(img, scale=2.0, mode='constant')
scale_in = skimage.transform.rescale(img, scale=0.5, mode='constant')

Don't forget to crop the images back to the original size(for scale out)
```

## 裁剪(Crop)

从原图中裁剪出一块并且resize到原图大小，它广为人知的形式是随机裁剪(random cropping).

```
Tensorflow. 'x' = A placeholder for an image

original_size = [h, w, c]
x = tf.placeholder(dtype=tf.float32, shape=origin_size)

crop_size=[h2, w2, c2]
x = tf.random_crop(x, size=crop_size, seed)
output = tf.images.resize_images(x, size=original_size)
```


## 平移(Translation)

将图像内容向各处移动。

> This method of augmentation is very useful as most objects can be located at almost anywhere in the image. This forces your convolutional neural network to look everywhere.


```
pad_left, pad_right, pad_top, pad_bottom denote the pixel 
displacement. Set one of them to the desired value and rest to 0
shape = [batch, height, width, channels]
x = tf.placeholder(dtype = tf.float32, shape = shape)

We use two functions to get our desired augmentation

x = tf.image.pad_to_bounding_box(x, pad_top, pad_left, height + pad_bottom + pad_top, width + pad_right + pad_left)
output = tf.image.crop_to_bounding_box(x, pad_bottom, pad_right, height, width)

```

## 噪声(Noise) Why adding noise might help with overcoming over-fitting?

高斯噪声/椒盐噪声  

```
#Numpy
img += np.random.normal(loc=0., scale=1.0, size=None)# Need truncated?


#TensorFlow. 'x' = A placeholder for an image.
shape = [height, width, channels]
x = tf.placeholder(dtype = tf.float32, shape = shape)
#Adding Gaussian noise
noise = tf.random_normal(shape=tf.shape(x), mean=0.0, stddev=1.0,
dtype=tf.float32)
output = tf.add(x, noise)
```

## Jitter 抖动

如何模拟这个现象？



## Seam Carving...



## Perspective  Transform

视点变换

## GAN



# 使用手册


What kind of data augmentation techniques are necessary?  

>It all depents on the case.  

我们的应用场景是怎样的，就如何构造数据。

但是如果数据不够，过拟合依然存在怎么办？


